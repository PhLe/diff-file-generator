# Diff-File generator

A Python script to generate automatically a diff-file for a LaTeX project based on `latex-diff` and `rsync`.
This script only runs in a Linux environment with Python 3.x.
