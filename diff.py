###################################################################
#
# Script to generate a diff between to .tex-projects
# It uses latexdiff and rsync for copying the folder structure.
#
# Author: Philipp Leise
# (c) 2021
# v 0.0.1
#
# usage: python diff.py oldproject newproject savediff
#
###################################################################

import os
import sys

# Get strings from input
old = str(sys.argv[1]) # "./paper_s1"
new = str(sys.argv[2]) # "./paper"
save = str(sys.argv[3]) #  "./Diff" 

# Paths
print(old)
print(new)
print(save)

# Copy folder strcture with rsync
os.system('rsync -av -f"+ */" -f"- *" "{}/" "{}"'.format(old, save))

# Files in paths
# old_files = os.listdir(old)
# new_files = os.listdir(new)

from pathlib import Path
path_tex_old = list(Path(old).rglob("*.tex"))
path_tex_new = list(Path(new).rglob("*.tex"))

base_old = {}
base_new = {}

for name in path_tex_old:
    base_old[os.path.basename(name)] = name

for name in path_tex_new:
    base_new[os.path.basename(name)] = name

# print(base_old)
# print(base_new)


# Generate diff-file for all equivalent files in old and new

for filename, path_old in base_old.items():
    # print(filename)
    # check if in new folder
    if filename in base_new.keys():
        savepath = os.path.join(save, os.path.relpath(path_old, old))
        os.system("latexdiff {} {} > {}".format(path_old, base_new[filename], savepath))












